import React from "react";
import { StyleSheet, View } from "react-native";
import AdvanceLogin from "./components/AdvanceLogin";
import ClickAndHold from "./components/ClickAndHold";

const styles = StyleSheet.create({
  container: {
    flex: 1,
    backgroundColor: "#8af",
    alignItems: "center",
    position: "relative",
    width: "100%",
  },
});

const App = () => {
  return (
    <View style={styles.container}>
      <ClickAndHold />
    </View>
  );
};

export default App;
