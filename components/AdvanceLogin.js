import React, { useEffect, useState } from "react";
import { StyleSheet, Text, Image, ScrollView, TextInput, Button } from "react-native";

// sample users username in database which must unique
const SAMPLE_USERNAME_DB = ["lio", "LIO", "1103194020", "TK4306"];

// checks new username to DB
const checkUserDB = (newUsername) => {
  for (let i = 0; i < SAMPLE_USERNAME_DB.length; i++) {
    if (SAMPLE_USERNAME_DB[i] === newUsername) {
      return false;
    }
  }
  return true;
};

const styles = StyleSheet.create({
  loginForm: {
    margin: 0,
    textAlign: "center",
    marginTop: 150,
    width: 300,
  },

  formTitle: {
    textAlign: "center",
    fontSize: 20,
  },

  inputElementInput: {
    borderBottomColor: "#000",
    height: 50,
    marginTop: 10,
    marginBottom: 10,
    padding: 2,
    borderBottomWidth: 2,
  },

  clickButton: {
    width: 100,
    height: 30,
    marginTop: 30,
  },

  reactImage: {
    width: 200,
    height: 200,
    marginLeft: 50,
    marginRight: 50,
    marginBottom: 20,
  },

  warnText: {
    color: "red",
    textAlign: "left",
    fontSize: 12,
    marginBottom: 5,
  },
});

const AdvanceLogin = () => {
  const [currentUsername, setCurrentUsername] = useState("");
  const [currentPassword, setCurrentPassword] = useState("");
  const [currentPasswordConf, setCurrentPasswordConf] = useState("");
  const [usernameWarn, setUsernameWarn] = useState(false);
  const [passwordWarn, setPasswordWarn] = useState(false);

  function usernameInputCallback() {
    // check to sample DB
    if (checkUserDB(currentUsername)) {
      // ok!, remove error if available
      setUsernameWarn(false);
    } else {
      // show error
      setUsernameWarn(true);
    }
  }

  const checkBothPasswords = () => {
    if (currentPassword === currentPasswordConf || currentPasswordConf === "") {
      // ok!, remove error if available
      setPasswordWarn(false);
    } else {
      // set error
      setPasswordWarn(true);
    }
  };

  const passwordRealCallback = (newRealPassword) => {
    setCurrentPassword(newRealPassword);

    // refresh conf pass
    setCurrentPasswordConf("");
  };

  const passwordConfirmCallback = (newConfirmPassword) => {
    setCurrentPasswordConf(newConfirmPassword);
  };

  useEffect(() => {
    checkBothPasswords();
  }, [currentPassword, currentPasswordConf]);

  return (
    <ScrollView style={styles.loginForm}>
      <Image style={styles.reactImage} source={require("../assets/react.png")} />
      <Text style={styles.formTitle}>Simple Register page</Text>

      <TextInput
        onBlur={usernameInputCallback}
        onChangeText={(newUsername) => setCurrentUsername(newUsername)}
        style={styles.inputElementInput}
        placeholder="Username"
      />
      {usernameWarn ? (
        <Text style={styles.warnText}>Username already used! use another!</Text>
      ) : null}

      <TextInput
        onChangeText={passwordRealCallback}
        secureTextEntry={true}
        style={styles.inputElementInput}
        placeholder="Password"
      />

      <TextInput
        onChangeText={passwordConfirmCallback}
        secureTextEntry={true}
        style={styles.inputElementInput}
        placeholder="Confirm Password"
      />
      {passwordWarn ? <Text style={styles.warnText}>Both Passwords don't match!</Text> : null}

      <Button style={styles.clickButton} title="Login" />
    </ScrollView>
  );
};

export default AdvanceLogin;
