import React, { useState } from "react";
import { StyleSheet, View, Text, Pressable, useWindowDimensions } from "react-native";

class Timer extends React.Component {
  constructor(props) {
    super(props);
    this.state = {
      currentTimeS: 5,
      currentTimeMS: 0,
      currentGameStatus: false,
      isGameOver: false,
    };
  }

  componentDidUpdate() {
    if (this.props.gameRunning !== this.state.currentGameStatus) {
      if (this.props.gameRunning) {
        this.setState({
          currentTimeS: 5,
          currentTimeMS: 0,
        });
        this.setTimer();
      } else {
        clearTimeout(this.timer);
        this.props.gameEndRecord(this.state.currentTimeS * 100 + this.state.currentTimeMS);
      }

      this.state.currentGameStatus = this.props.gameRunning;
    }
  }

  setTimer() {
    let interval = 5; // leap every 10 ms
    let expected = Date.now() + interval;
    this.timer = setTimeout(() => {
      step();
    }, interval);

    const step = () => {
      let dt = Date.now() - expected;

      if (
        this.state.currentTimeS > 0 ||
        (this.state.currentTimeS == 0 && this.state.currentTimeMS >= 1)
      ) {
        if (this.state.currentTimeMS > 0) {
          this.setState((state, props) => {
            return { currentTimeMS: state.currentTimeMS - 1 };
          });
        } else {
          this.setState((state, props) => {
            return { currentTimeS: state.currentTimeS - 1, currentTimeMS: 99 };
          });
        }

        expected += interval;
        this.timer = setTimeout(step, Math.max(0, interval - dt));
      } else {
        this.props.gameOverCallback();
      }
    };
  }

  render() {
    return (
      <View style={styles.timerWrapper}>
        <Text style={styles.timerText}>
          0{this.state.currentTimeS}: {this.state.currentTimeMS.toString().length == 1 ? "0" : ""}
          {this.state.currentTimeMS}
        </Text>
      </View>
    );
  }
}

const BestBoard = (props) => {
  let time = props.time;

  if (time > 0) {
    time = time / 100;
    let [timeS, timeMS] = time.toString().split(".");

    return (
      <View style={styles.bestBoard}>
        <Text style={styles.bestBoardText}>
          Best: 0{timeS} : {timeMS.length < 2 ? "0" : ""} {timeMS}
        </Text>
      </View>
    );
  }

  return null;
};

const GameText = (props) => {
  if (props.gameOver) {
    return <Text style={styles.gameText}>Time out!</Text>;
  }

  if (props.gameRunning) {
    return <Text style={styles.gameText}>Release button at:</Text>;
  } else {
    return <Text style={styles.gameText}>Hold Button to Start</Text>;
  }
};

const ButtonClick = (props) => {
  return (
    <Pressable
      onLongPress={props.startGame}
      onPressOut={props.endGame}
      style={styles.mainButton}
      android_ripple={{ color: "#123" }}
    >
      <Text style={styles.mainButtonText}>
        {props.gameRunning ? "Release me!" : "Hold to Start!"}
      </Text>
    </Pressable>
  );
};

const ClickAndHoldGame = () => {
  const [currentBestMS, setCurrentBestMS] = useState(0);
  const [gameRunning, setGameRunning] = useState(false);
  const [gameOver, setGameOver] = useState(false);

  const setNewRecord = (newRecord) => {
    if (newRecord !== 0) {
      if (newRecord < currentBestMS || currentBestMS == 0) {
        setCurrentBestMS(newRecord);
      }
    }
  };

  const endGame = () => {
    setGameRunning(false);
  };

  const startGame = () => {
    setGameRunning(true);
    setGameOver(false);
  };

  const gameOverGame = () => {
    setGameOver(true);
  };

  return (
    <View style={styles.gameWrapper}>
      <View style={styles.gameTop}>
        <Text style={styles.gameTitle}>precision game</Text>
        <BestBoard time={currentBestMS}></BestBoard>
      </View>

      <View style={styles.gameCenter}>
        <GameText gameRunning={gameRunning} gameOver={gameOver}></GameText>
        <Timer
          gameRunning={gameRunning}
          gameOverCallback={gameOverGame}
          gameEndRecord={setNewRecord}
        ></Timer>
      </View>

      <View style={styles.gameBottom}>
        <ButtonClick
          gameRunning={gameRunning}
          startGame={startGame}
          endGame={endGame}
          gameOverStatus={gameOver}
        ></ButtonClick>
      </View>
    </View>
  );
};

export default ClickAndHoldGame;

const styles = StyleSheet.create({
  gameWrapper: {
    textAlign: "center",
    flexDirection: "column",
  },

  // top section
  gameTop: {
    marginTop: 50,
    flex: 1,
  },

  gameTitle: {
    fontSize: 30,
    textAlign: "center",
  },

  bestBoard: {
    marginBottom: "25%",
  },

  bestBoardText: {
    textAlign: "center",
  },

  // center section
  gameCenter: {
    flex: 8,
    flexDirection: "column",
    alignItems: "center",
    justifyContent: "center",
  },

  gameText: {
    textAlign: "center",
  },

  timerText: {
    fontSize: 45,
    padding: 20,
    textAlign: "center",
  },

  // bottom section
  gameBottom: {
    flex: 1,
    textAlign: "center",
  },

  mainButton: {
    backgroundColor: "#5585ff",
    padding: 20,
    paddingLeft: 30,
    paddingRight: 30,
  },

  mainButtonText: {
    textAlign: "center",
  },
});
